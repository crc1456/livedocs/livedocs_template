# LiveDocs template
## Badges
### Private CRC Binderhub Links
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Flivedocs_template/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Flivedocs_template/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Flivedocs_template/HEAD?urlpath=voila)

### Public CRC Binderhub Links
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Flivedocs_template/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Flivedocs_template/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2Flivedocs_template/HEAD?urlpath=voila)

### JupyterLite Link
[![lite-badge](https://img.shields.io/badge/CRC1456-Jupyterlite-yellow)](https://crc1456.pages.gwdg.de/livedocs/livedocs_template)

### Static HTMLs
[![static-html](https://img.shields.io/badge/CRC1456-sample_jupyter_notebook-white)](https://crc1456.pages.gwdg.de/livedocs/livedocs_template/files/sample_jupyter_notebook.html)

### Docker Image at GWDG Gitlab Docker Registry
[![Docker](https://img.shields.io/badge/CRC1456-Dockerhub-blue)](https://gitlab.gwdg.de/crc1456/livedocs/livedocs_template/container_registry/2353)

-----

## Description
This is the starting point for creating a LiveDocs version of your git repository.

## Limitations
So far, this repository and its README file are intended to guide you through the process of converting a **python repository** (i.e., a repository with python scripts) into a LiveDocs repository. Other languages can be used in LiveDocs, but are not fully supported. If you want to convert your repository in other language into a LiveDoc, please contact us at `crc1456-inf-private at uni-goettingen.de` or `p.klein at math.uni-goettingen.de`.

## Configuration

The configuration of a LiveDoc is, in fact, the preparation of your git repository for executing in the CRC1456 Binderhub server ([Private](http://livedocs-private.math.uni-goettingen.de:30901) or [Public](http://livedocs.math.uni-goettingen.de:30901), depending on the availability of your repository) and as a [Jupyterlite](https://jupyterlite.readthedocs.io/en/latest/) service, using gitlab pages.

### 1) Getting started
The first thing to do is downloading this repository to your local machine and creating a new folder for your own repository with its contents. If you have already a repository, then copy the files from this repository into your repository's folder or create a new repository to "merge" both data. Notice that if your repository has already a requirements.txt file or a .ci-gitlab.yml file, for instance, you might need to merge those files.

### 2) Setting up your LiveDocs repository
Once you have everything together (your code and the files from this repo), you now need to customize some files in order to prepare your repository:
- requirements.txt: This is a file containing all the python packages to be installed in your LiveDocs. Add to this file a list of the python packages required to run your application. If necessary, specify the version of the packages along with its name using 'PACKAGE==VERSION'. Please note that in this file there are already some example libraries, you can change this document as you wish and remove any libraries from the examples that are not needed.
- jupyterlite-requirements.txt: similar to `requirements.txt`, this file contains the requirementes specific to jupyterlite build. You don't need to change anything here, unless you want to customize your jupyterlite building.
- postBuild: This is a file which contains all the steps to be run after building your repo into a LiveDoc. Add to this file a list of commands that you want to run after the building of your repo. Note that there are already some commands in this file, please do not remove those commands unless you are 100% sure of what you are doing.
- .gitlab-ci.yml: This YAML file contains the commands for all the CI routines. You don't need to modify anything in this file, except for the branch name in the last line (below the "only" flag) if you want to build jupyterlite a branch other than master (e.g. you have a branch named 'main' instead of 'master' as the principal branch, or you want to have a specific branch for jupyterlite code).

Other non-mentioned files are not intended to be altered, so make any alterations at your own risk.

### 3) Configuring CI/CD in GWDG gitlab:
Once you have uploaded your repository to GWDG Gitlab, you need to enable CI/CD in Setting so it can run the CI routines for enabling Jupyterlite.

### 4) Updating this README.md file:
Once your repository is good to go, you should be able to use the [badges](#badges) at the top of this document to access it. However, you first need to update the links, so they are built in our servers.

To alter the Binderhub links, you first need to decide whether you will use a private or a public server:
- The [Public CRC1456 Binderhub Server](http://livedocs.math.uni-goettingen.de:30901) requires your repository to be public, and is intended for divulgation of your work to the general audience. It has no access restriction.
- The [Private CRC1456 Binderhub Server](http://livedocs-private.math.uni-goettingen.de:30901) has its access restricted to GWDG Gitlab members that are in the CRC1456 Gitlab group (please require access if you are not in the group already using the aforementioned e-mails). This server does not require you to have a public repository, and is intended for you to share your results with other CRC1456 members.

Once decided the server, you need to update the links in the badges following the current format:
- For Binderhub with Jupyterlab: `http://<SERVER_IP>/v2/gwdg/crc1456%2Flivedocs%2F<REPOSITORY_NAME>/HEAD`
- For Binderhub with Jupyter Classic: `http://<SERVER_IP>/v2/gwdg/crc1456%2Flivedocs%2F<REPOSITORY_NAME>/HEAD?urlpath=tree)`
- For Binderhub with Voila: `http://<SERVER_IP>/v2/gwdg/crc1456%2Flivedocs%2F<REPOSITORY_NAME>/HEAD?urlpath=voila)`
- For Jupyterlite: `https://crc1456.pages.gwdg.de/livedocs/<REPOSITORY_NAME>)`

Please notice that this alteration is at the second link on the badge (in parenthesis), e.g.: <br>
`[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](>>>THIS IS THE PART YOU CHANGE<<<)`

Additionally, you can also enable/disable the generation of static HTML files from your jupyter notebooks in the CI YAML file. The files can be linked then using the same structure from the latest badge (Static HTML), altering the file name to match the desired HTML file. The generated HTML files are homonymous to the Jupyter Notebook files present in the repo, but with a .html extension.

E.g.: If you have a file "somenotebook.ipynb" in your repo, the link to it, after the CI/CD execution should be <br>
`https://crc1456.pages.gwdg.de/<REPO GROUP>/<REPO NAME>/files/somenotebook.html` (replacing the tags for your repository group and repository name)

### 5) Docker Containers
This repository is configured for creating a docker image of it in the GWDG Gitlab Docker Registry. The CI scripts deploy a jupyterhub image copying the content of this repository to it, so it can be run independently. For such, you need to activate the repository's container registry (General, Settings, expand "Visibility, project features and permissions" and enable Container Registry).

This process can be commented or taken out of the CI YAML file if you do not wish for your repository to be available as a docker image.

You can customize the Dockerfile to suit your needs, but keep in mind that the same Dockerfile is used to launch your repository in Binderhub, so alter it at your own risk. It is adivsable to test launching the Binderhub instances after changing the Dockerfile to check if everything is ok.

The docker container is pushed to the GWDG Docker registry (docker.gitlab.gwdg.de), and the docker image should have the same name of your repository (e.g. livedocs_template).

To use it locally, you must first pull it from the registry. For such, you must first log into the GWDG docker registry with `docker login docker.gitlab.gwdg.de` and then download the image using the `docker pull docker.gitlab.gwdg.de/<PATH TO YOUR REPO>` (e.g docker pull docker.gitlab.gwdg.de/crc1456/livedocs/livedocs_template). By default, you can run your docker image in a container using something like the following command (replacing the livedocs_template by the name of your image):
`docker run --rm -p 8889:8888 --name livedocs_test docker.gitlab.gwdg.de/crc1456/livedocs/livedocs_template`

In this case, you can access your jupyter notebook server at http://localhost:8889 and the password to use it is _crc1456livedoc_ (this can be changed in the Dockerfile).

## Contact us

In case you need any support, please contat us at `crc1456-inf-private at uni-goettingen.de` or `p.klein at math.uni-goettingen.de`.
